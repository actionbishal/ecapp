import 'package:flutter/material.dart';

AppBar cardPaymentAppBar() {
  return AppBar(

    backgroundColor: Colors.white,
    elevation: 1,
    title: Text("Credit/Debit Card",style: TextStyle(fontSize: 16),),
  );
}
