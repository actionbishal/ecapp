import 'package:flutter/material.dart';

AppBar checkoutAppBar() {
  return AppBar(
    backgroundColor: Colors.white,

    elevation: 1,
    title: Text("Checkout"),
  );
}
